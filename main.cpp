#include "mbed.h"
#include "FXOS8700Q.h"
#include "EthernetInterface.h"
#include <stdio.h>
//#include "Socket.h"
#include "TCPSocket.h"
#include <TCPServer.h>

#define MBED_DEV_IP       "192.168.0.52"
#define MBED_DEV_MASK     "255.255.255.0"
#define MBED_DEV_GW       "0.0.0.0"
#define SERVER_PORT   5000
#define BUFFER_SIZE 512
//FXOS8700Q acc( A4, A5, FXOS8700CQ_SLAVE_ADDR0); // Proper Ports and I2C address for Freescale Multi Axis shield
//FXOS8700Q mag( A4, A5, FXOS8700CQ_SLAVE_ADDR0); // Proper Ports and I2C address for Freescale Multi Axis shield
FXOS8700Q_acc acc( PTE25, PTE24, FXOS8700CQ_SLAVE_ADDR1); // Proper Ports and I2C Address for K64F Freedom board
FXOS8700Q_mag mag( PTE25, PTE24, FXOS8700CQ_SLAVE_ADDR1); // Proper Ports and I2C Address for K64F Freedom board

//Serial pc(USBTX, USBRX);

MotionSensorDataUnits mag_data;
MotionSensorDataUnits acc_data;

EthernetInterface eth;
TCPSocket socket;
TCPServer server;

int main() {
    eth.set_network(MBED_DEV_IP, MBED_DEV_MASK, MBED_DEV_GW);
    eth.connect();
    printf("Mi IP es: %s\n", eth.get_ip_address());

    server.open(&eth);
    server.bind(SERVER_PORT);
    server.listen(1);

    int n;
    acc.enable();
    while (true) {
        TCPSocket client;
        SocketAddress client_addr;
        client.set_blocking(true);
        printf("Esperando por conexion...\n");
        n = server.accept(&client, &client_addr);
        if (n < 0) continue;

        printf("Conexion establecida desde: %s\n", client_addr.get_ip_address());
        char buffer[BUFFER_SIZE];
        n = 1;
        while (n > 0) {
            acc.getAxis(acc_data);
            mag.getAxis(mag_data);
            sprintf(buffer, "ACC: X=%1.4f Y=%1.4f Z=%1.4f - MAG: X=%4.1f Y=%4.1f Z=%4.1f",
            acc_data.x, acc_data.y, acc_data.z, mag_data.x, mag_data.y, mag_data.z);
            n = client.send(buffer, strlen(buffer));
            wait(3.0);
        }
        client.close();
    }
}